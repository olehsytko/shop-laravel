<?php

use Illuminate\Support\Facades\Route;

\Illuminate\Support\Facades\Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false
]);

Route::get('/', 'MainController@index')->name('index');
Route::get('/categories', 'MainController@categories')->name('categories');

Route::get('/basket', 'OrderController@basket')->name('basket');
Route::get('/basket/place', 'OrderController@basketPlace')->name('basket-place');
Route::post('/basket/confirm', 'OrderController@basketConfirm')->name('basket-confirm');
Route::post('/basket/add/{id}', 'OrderController@basketAdd')->name('basket-add');
Route::post('/basket/remove/{id}', 'OrderController@basketRemove')->name('basket-remove');

Route::get('/category/{category}', 'MainController@category')->name('category');
Route::get('/{category}/{product}', 'MainController@product')->name('product');
